#! /usr/bin/env python

import csv
import datetime
import time

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import speedtest
import typer

tester = speedtest.Speedtest()
app = typer.Typer()


@app.command()
def run(
    interval: int = typer.Option(60 * 30, help="time in sec to sleep between tests"),
    save: bool = typer.Option(False, help="keep registers in csv"),
):
    """Run speed test monitor"""

    with open("test.csv", mode="w") as speedcsv:
        csv_writer = csv.DictWriter(
            speedcsv, fieldnames=["time", "downspeed", "upspeed"]
        )
        csv_writer.writeheader()
        while True:
            time_now = datetime.datetime.now().strftime("%H:%M:%S")
            downspeed = round((round(tester.download()) / 1048576), 2)
            upspeed = round((round(tester.upload()) / 1048576), 2)

            if save:
                csv_writer.writerow(
                    {"time": time_now, "downspeed": downspeed, "upspeed": upspeed}
                )
            else:
                typer.echo(
                    f"time: {time_now}, downspeed: {downspeed} Mb/s, upspeed: {upspeed} Mb/s"
                )
            time.sleep(interval)


@app.command()
def plot():
    times = []
    download = []
    upload = []

    with open("test.csv", "r") as csvfile:
        plots = csv.reader(csvfile, delimiter=",")
        next(csvfile)
        for row in plots:
            times.append(str(row[0]))
            download.append(float(row[1]))
            upload.append(float(row[2]))

    plt.figure(figsize=(30, 30))
    plt.plot(times, download, label="download", color="r")
    plt.plot(times, upload, label="upload", color="b")
    plt.xlabel("time")
    plt.ylabel("speed in Mb/s")
    plt.title("internet speed")
    plt.legend()
    plt.savefig("test_graph.jpg", bbox_inches="tight")


if __name__ == "__main__":
    typer.echo("Running internet connection monitor")
    app()
